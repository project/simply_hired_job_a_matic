<?php
/**
 * @file
 * Default theme implementation for individual job post.
 *
 * Available variables:
 *
 * $job - Object with the following properties:
 *
 *     $job_title Title for the job listing (string)
 *     $company Company name that posted the job listing. (string)
 *     $source Source for the job listing (array or FALSE if no source)
 *       if an array, will contain the following indexes:
 *         $name - source name
 *         $url - URL locator for the job posting
 *     $type Type of job posting (string)
 *       will be one of the following:
 *         paid - posted directly to the site for a flat fee
 *         sponsored - generate revenue on a CPC basis
 *         organic - generate revenue on an affiliate basis
 *     $location Associative array with the following keys:
 *       cty - city
 *       st - state
 *       postal - postal code
 *       county - Unknown; API currently returns an empty string
 *       region - Unknown; API currently returns an empty string
 *       country - country; unknown if SimplyHired supports non-US
 *                 job postings.
 *     $last_seen - The date that the job was last seen; verified to
 *                  be an active listing (int)
 *     $date_posted The date that the job was first posted. (int)
 *     $excerpt Excerpt of the job description (string)
 *     $zebra even/odd for the job row in the recordset
 */
?>
<div id="jobamatic-job-<?php print $job->date_posted; ?>" class="jobamatic-job jobamatic-job-<?php print $job->date_posted; ?> <?php print $job->zebra; ?>">
  <div class="jobamatic-header">
    <h3><?php print $job->job_title; ?></h3>
    <h4 class="jobamatic-company">
      <?php print $job->company; ?>
    </h4>
    <h4 class="jobamatic-post-date">Posted on <?php print date('M j, Y g:i A', $job->date_posted); ?></h4>
  </div>
  <div class="content">
    <p><strong><?php print t('Location'); ?>: </strong><?php print $job->location['cty'] . ', ' . $job->location['st'] . ' ' . $job->location['postal']; ?></p>
    <p><strong><?php print t('Job description'); ?>:</strong> <?php print $job->excerpt; ?></p>
    <p class="jobamatic-more"><?php print l(t('View full job posting'), $job->source['url'], array('attributes' => array('target' => '_blank'))); ?></p>
  </div>
</div>
