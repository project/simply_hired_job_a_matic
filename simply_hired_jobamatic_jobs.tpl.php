<?php
/**
 * @file
 * Default theme implementation for job search results.
 *
 * Available variables:
 *
 * $results - array of jobs result set retrieved from the SimplyHired API
 */
?>
<div id="jobamatic-search-resultset" class="jobamatic-search-resultset">
  <h3><?php print $results['title']; ?></h3>
  <?php foreach ($results['items'] as $job): ?>
    <?php print theme('simply_hired_jobamatic_job', array('job' => $job)); ?>
  <?php endforeach; ?>
</div>
