<?php
/**
 * @file
 * SimplyHired's Job-a-matic REST API implementation.
 *
 * Executes calls to SimplyHired's Job-a-matic API and returns
 * formatted results for use within Drupal.
 */

define('JOBAMATIC_ENDPOINT', 'http://api.simplyhired.com/a/jobs-api/xml-v2/');
define('JOBAMATIC_XML_PARSERERROR', 1001);

class SimplyHiredJobamaticAPI {
  protected $pshid;
  protected $jbd;
  protected $ssty;
  protected $cflg;
  protected $clip;
  protected $error;
  protected $code;
  protected $returnerrors;

  /**
   * Constructor.
   */
  public function __construct($pshid, $jbd, $returnerrors = FALSE) {
    $this->pshid = $pshid;
    $this->jbd = $jbd;
    $this->ssty = 2;
    $this->cflg = 'r';
    $this->error = FALSE;
    $this->code = NULL;
    $this->returnerrors = $returnerrors;
  }

  /**
   * Sets the client IP address.
   */
  public function setClip($clip) {
    $this->clip = trim($clip);
  }

  /**
   * Returns web service call status code.
   */
  public function getCode() {
    return intval($this->code) != 0 ? intval($this->code) : -1000;
  }

  /**
   * Returns class error message.
   */
  public function getError() {
    return $this->error;
  }

  /**
   * Prepare and call Jobamatic search service.
   */
  public function search($query, $location = '', $miles = 5, $sort = 'rd', $size = 10, $page = 0) {
    $params = array(
      'q' => urlencode(trim($query)),
      'sb' => $sort,
      'ws' => $size,
      'pn' => (intval($page) < 1 ? 0 : intval($page)),
    );

    if (!is_null($location) && $location != '') {
      $params['l'] = $location;
    }

    if (!is_null($location) && intval($miles) > 0) {
      $params['m'] = $miles;
    }

    $results = $this->call($params);

    return $results;
  }

  /**
   * Protected function that executes the web service request.
   *
   * @access protected
   */
  protected function call($criteria) {
    $data = FALSE;
    $url = JOBAMATIC_ENDPOINT . '%s?';
    $api_identity = array();

    foreach ($criteria as $key => $value) {
      $api_identity[] = $key . '-' . $value;
    }

    $params = array(
      'pshid' => $this->pshid,
      'jbd' => $this->jbd,
      'ssty' => $this->ssty,
      'clip' => $this->clip,
      'cflg' => $this->cflg,
    );
    $param_string = array();
    foreach ($params as $key => $value) {
      $param_string[] = $key . '=' . $value;
    }

    $url .= implode('&', $param_string);
    $url = sprintf($url, implode('/', $api_identity));

    $result = drupal_http_request($url);

    $this->code = $result->code;

    if ($this->code != 200) {
      $this->error = $result->error;
      if ($this->returnerrors === TRUE) {
        $u = l($url, $url);
        drupal_set_message($this->error . '; URL: @url', 'error');
      }
      watchdog('jobamatic', $this->error, array('@url' => $u), WATCHDOG_ERROR, $url);
    }
    else {
      $data = $this->parseXML($result->data);
    }

    return $data;
  }

  /**
   * Parses returned xml from web service calls.
   */
  protected function parseXML($xml) {
    $xml = @simplexml_load_string($xml);

    if ($xml !== FALSE) {
      // No error tag.
      if ($xml->error == '') {
        $data = array(
          'title' => (string) $xml->rq->t,
          'start_index' => (int) $xml->rq->si,
          'num_results' => (int) $xml->rq->rpd,
          'total_results' => (int) $xml->rq->tr,
          'total_visible' => (int) $xml->rq->tv,
          'items' => array(),
        );

        $record_set = $xml->rs;

        $idx = 1;
        /*
        * Create an object out of each of the XML recordset records
        * for easier handeling.
        */
        foreach ($record_set->r as $record) {
          $obj = new stdClass();
          $obj->job_title = (string) $record->jt;
          $obj->company = (string) $record->cn;
          if ($record->src) {
            $src = array('name' => (string) $record->src);
            // Grab all of the XML attributes for the record source
            // so they can be used.
            foreach ($record->src->attributes() as $key => $value) {
              $src[$key] = (string) $value;
            }
            $obj->source = $src;
          }
          else {
            $obj->source = FALSE;
          }

          $obj->type = (string) $record->ty;
          $loc = array();
          foreach ($record->loc->attributes() as $key => $value) {
            $loc[$key] = (trim($value) == '' ? NULL : trim($value));
          }
          // City is empty if the state is DC, so populate the city variable
          // so we have the correct display.
          if ($loc['st'] == 'DC' && is_null($loc['cty'])) {
            $loc['cty'] = 'Washington';
          }
          // Job location.
          $obj->location = $loc;
          $obj->last_seen = strtotime((string) $record->ls);
          $obj->date_posted = strtotime((string) $record->dp);
          $obj->excerpt = (string) $record->e;
          $obj->zebra = $idx % 2 == 0 ? 'even' : 'odd';
          $data['items'][] = $obj;
          $idx++;
        }

      }
      else {
        $data = array(
          'title' => t('No Results'),
          'start_index' => 0,
          'num_results' => 0,
          'total_results' => 0,
          'total_visible' => 0,
          'items' => array(),
        );
      }

      return $data;
    }
    else {
      $this->error = t('Unable to parse XML response from Simply Hired.');
      $this->code = JOBAMATIC_XML_PARSERERROR;

      if ($this->returnerrors === TRUE) {
        drupal_set_message($this->error, 'error');
      }
      watchdog('jobamatic', $this->error, array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

}
