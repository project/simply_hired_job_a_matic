-- SUMMARY --

Job-a-matic is a great way to earn additional revenue from your existing website
and provide valuable content for your users with a free hosted job board. The
Job-a-matic module uses SimplyHired's XML API to display job postings from their
database directly within your Drupal site.

For a full description of the module, visit the project page:
  {project url goes here}

To submit bug reports and feature suggestions, or to track changes:
  {issue reports url goes here}


To see the full list of features and benefits of Job-a-matic, visit the
Job-a-matic website:
  http://www.jobamatic.com

NOTICE: The SimplyHired Terms of Service requires the attribution block on any
page that displays SimplyHired data. This can be disabled in the module
configuration, but By doing so, you knowingly violate the SimplyHired
Terms of Service Agreement and the module author is not liable for any legal
action taken by SimplyHired. The official terms are located at
http://www.jobamatic.com/jbb-static/terms-of-service.

-- REQUIREMENTS --

None.

-- INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* You must create an account with SimplyHired prior to using this module. To
  create an account, go to
  https://www.jobamatic.com/a/jbb/partner-register-account.

* Once your account has been created, log in an click on the "Advanced" tab.
  Under the Job Search GET Parameters section, locate your publisher ID and
  Job-a-matic domain. (The other parameters are passed automatically by the
  module in api calls.)

* On your Drupal site, log in as a user with administer site configuration
  permission and go to Configuration > Job-a-matic Settings and enter your
  publisher ID and Job-a-matic domain.

* Enter the Default Search Criteria that will be used to search SimplyHired's
  database to populate your jobs page.

* Adjust any other configuration settings to your liking.

-- CUSTOMIZATION --

* To view your job page, you can browse to
  http://yourdomain.com/content/simply_hired_jobamatic/view
  You will probably want to set up a path alias (if you are using the
  path module) to give your job page a more SEO friendly URL.

* You will also need to manually add any menu links that you need by editing the
  appropriate menus under Structure > Menus.

-- TROUBLESHOOTING --

None.

-- FAQ --

None.

-- CONTACT --

Current maintainers:
* Ron Ferguson (r0nn1ef) - http://drupal.org/user/290065
