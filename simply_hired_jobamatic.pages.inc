<?php
/**
 * @file
 * Adds SimplyHired's Job-a-matic module specific callback functions.
 */

  /**
   * Menu callback function to render the job page.
   */
function simply_hired_jobamatic_render_page() {

  $defaults = variable_get('simply_hired_jobamatic_defaults', '');
  if (!is_array($defaults)) {
    $defaults = simply_hired_jobamatic_get_defaults();
  }

  $data = array();

  drupal_set_title(variable_get('simply_hired_jobamatic_page_title', 'Jobs'));

  if ($defaults['allow_custom_search']) {
    $data['form'] = drupal_get_form('simply_hired_jobamatic_custom_search_form');
  }

  $data['wrapper'] = array(
    '#type' => 'markup',
    '#markup' => '<div id="jobamatic-search-results"></div>',
  );

  if ($defaults['include_attribution']) {
    $logo_url = drupal_get_path('module', 'simply_hired_jobamatic') . '/Logo_SHpartner.png';
    $logo = '<img src="' . url($logo_url, array('absolute' => TRUE)) . '" height="20" width="150" alt="'
      . t('SimplyHired') . '" />';

    $data['attribution'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="simplyhired-attribution">',
      '#suffix' => '</div>',
      '#markup' => l($logo, 'http://www.simplyhired.com', array('html' => TRUE, 'attributes' => array('target' => '_blank'))),
    );
  }

  $data['#attached']['css'][] = drupal_get_path('module', 'simply_hired_jobamatic') . '/css/jobamatic.css';
  $data['#attached']['js'][] = drupal_get_path('module', 'simply_hired_jobamatic') . '/js/jobamatic.js';

  return $data;
}

/**
 * Build the search form.
 */
function simply_hired_jobamatic_custom_search_form($form, &$form_state) {
  $defaults = variable_get('jobamatic_defaults', '');
  if (!is_array($defaults)) {
    $defaults = simply_hired_jobamatic_get_defaults();
  }

  $form['jsq'] = array(
    '#type' => 'textfield',
    '#title' => t('Search criteria'),
    '#default_value' => '',
    '#description' => t('Enter your job search criteria.'),
    '#maxlength' => 256,
  );

  $form['advanced_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Search Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => FALSE,
  );

  $form['advanced_search']['location'] = array(
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#description' => t('City/State to confine the search to. Can be either city/state or zip code. Search currently does not support multiple locations.'),
    '#default_value' => '',
  );

  $options = array(
    '5' => t('5 miles'),
    '10' => t('10 miles'),
    '25' => t('25 miles'),
    '50' => t('50 miles'),
    '100' => t('100 miles'),
  );

  $form['advanced_search']['miles'] = array(
    '#type' => 'select',
    '#title' => t('Search radius in miles'),
    '#options' => $options,
    '#default_value' => $defaults['miles'],
  );

  $sorts = array(
    'rd' => t('relevance descending (default)'),
    'ra' => t('relevance ascending'),
    'dd' => t('last seen date descending'),
    'da' => t('last seen date ascending'),
    'td' => t('title descending'),
    'ta' => t('title ascending'),
    'cd' => t('company descending'),
    'ca' => t('company ascending'),
    'ld' => t('location descending'),
    'la' => t('location ascending'),
  );

  $form['advanced_search']['sort_by'] = array(
    '#type' => 'select',
    '#title' => t('Sort By'),
    '#description' => '',
    '#default_value' => $defaults['sort_by'],
    '#options' => $sorts,
  );

  $form['advanced_search']['window_size'] = array(
    '#type' => 'select',
    '#title' => t('Jobs per page'),
    '#default_value' => $defaults['window_size'],
    '#options' => drupal_map_assoc(array(10, 20, 50, 75, 100)),
  );

  $form['submit'] = array(
    '#type' => 'button',
    '#value' => t('Search'),
  );

  return $form;
}
