<?php
/**
 * @file
 * Administration page callbacks for the jobamatic module.
 */

/**
 * Admin form callback.
 */
function simply_hired_jobamatic_admin_settings() {
  $form = array();

  $form['jobamatic_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account Settings'),
    '#tree' => FALSE,
  );

  $pshid_msg = t('You can obtain this information by logging in to the
<a href="@portal_url" target="_jobamatic">Jobamatic portal</a>
then clicking on the <a href="@xml_url" target="_jobamatic">XML API tab</a>.',
  array(
    '@portal_url' => url('https://www.jobamatic.com/a/jbb/partner-login', array('external' => TRUE)),
    '@xml_url' => url('https://www.jobamatic.com/a/jbb/partner-dashboard-advanced-xml-api', array('external' => TRUE)),
  ));

  $form['jobamatic_account']['simply_hired_jobamatic_pshid'] = array(
    '#type' => 'textfield',
    '#title' => t('Publisher ID'),
    '#default_value' => variable_get('simply_hired_jobamatic_pshid', ''),
    '#description' => $pshid_msg,
  );

  $form['jobamatic_account']['simply_hired_jobamatic_jbd'] = array(
    '#type' => 'textfield',
    '#title' => t('Job-a-matic Domain'),
    '#default_value' => variable_get('simply_hired_jobamatic_jbd', ''),
    '#description' => t('You Job-a-matic domain. No protocol or slashes. See the XML API tab of the Job-a-matic portal for this information.'),
  );

  $form['jobamatic_defaults'] = array(
    '#title' => t('Default Settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
  );

  $form['jobamatic_defaults']['simply_hired_jobamatic_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Title'),
    '#default_value' => variable_get('simply_hired_jobamatic_page_title', t('Jobs')),
    '#description' => t('Set the page title of the jobs display page.'),
  );

  $defaults = variable_get('simply_hired_jobamatic_defaults', '');

  if (!is_array($defaults)) {
    $defaults = simply_hired_jobamatic_get_defaults();
  }

  $form['simply_hired_jobamatic_defaults']['query'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Search Criteria'),
    '#default_value' => $defaults['query'],
    '#maxlength' => 500,
  );

  $form['simply_hired_jobamatic_defaults']['location'] = array(
    '#type' => 'textfield',
    '#title' => t('Location'),
    '#description' => t('City/State to confine the search to. Can be either city/state or zip code. SimplyHired API currently does not support multiple locations.'),
    '#default_value' => $defaults['location'],
  );

  $options = array(
    '5' => '5 miles',
    '10' => '10 miles',
    '25' => '25 miles',
    '50' => '50 miles',
    '100' => '100 miles',
  );

  $form['simply_hired_jobamatic_defaults']['miles'] = array(
    '#type' => 'select',
    '#title' => t('Search radius in miles'),
    '#options' => $options,
    '#default_value' => $defaults['miles'],
  );

  $sorts = array(
    'rd' => 'relevance descending (default)',
    'ra' => 'relevance ascending',
    'dd' => 'last seen date descending',
    'da' => 'last seen date ascending',
    'td' => 'title descending',
    'ta' => 'title ascending',
    'cd' => 'company descending',
    'ca' => 'company ascending',
    'ld' => 'location descending',
    'la' => 'location ascending',
  );

  $form['simply_hired_jobamatic_defaults']['sort_by'] = array(
    '#type' => 'select',
    '#title' => t('Sort By'),
    '#description' => '',
    '#default_value' => $defaults['sort_by'],
    '#options' => $sorts,
  );

  $form['simply_hired_jobamatic_defaults']['window_size'] = array(
    '#type' => 'select',
    '#title' => t('Jobs per page'),
    '#default_value' => $defaults['window_size'],
    '#options' => drupal_map_assoc(array(10, 20, 50, 75, 100)),
  );

  $form['simply_hired_jobamatic_defaults']['allow_custom_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable custom search'),
    '#description' => t('If enabled, a form will be added to the jobs page that allows site visitors to enter custom job search criteria.'),
    '#default_value' => $defaults['allow_custom_search'],
  );

  $attribs = array('attributes' => array('target' => '_blank'));
  $replacements = array(
    '!tos' => l(t('Terms of Service Agreement'), 'http://www.jobamatic.com/jbb-static/terms-of-service', $attribs),
  );
  $attrib_desc = t('The SimplyHired !tos states that this must be included on any page that displays SimplyHired data.', $replacements);
  $attrib_desc .= '<br /><strong>' . t('By disabling this, you knowingly violate the SimplyHired Terms of Service Agreement and the module author is not liable for any legal action taken by SimplyHired.') . '</strong>';

  $form['simply_hired_jobamatic_defaults']['include_attribution'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include SimplyHired attribution?'),
    '#description' => $attrib_desc,
    '#default_value' => $defaults['include_attribution'],
  );

  $form['#validate'][] = 'simply_hired_jobamatic_admin_settings_validate';

  $form['#submit'][] = 'simply_hired_jobamatic_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Settings form validation.
 */
function simply_hired_jobamatic_admin_settings_validate(&$form, &$form_state) {
  $pshid = trim($form_state['values']['simply_hired_jobamatic_pshid']);
  $pshid_exp = '/^[0-9]{3,}$/';
  if ($pshid != '' && preg_match($pshid_exp, $pshid) !== 1) {
    form_set_error('simply_hired_jobamatic_pshid', t('Publisher ID must contain only numbers and must be at least three characters in length.'));
  }

  // @TODO: Probably should validate the jobamatic domain,
  // but can't get any feedback from simplyhired on what
  // exactly is a valid domain...don't want to assume even
  // though it is logical what it would be.
}

/**
 * Callback function that processes the admin settings for submissions.
 */
function simply_hired_jobamatic_admin_settings_submit($form, &$form_state) {
  $pshid = trim($form_state['values']['simply_hired_jobamatic_pshid']);
  $jbd = trim($form_state['values']['simply_hired_jobamatic_jbd']);
  variable_set('simply_hired_jobamatic_pshid', $pshid);
  variable_set('simply_hired_jobamatic_jbd', $jbd);

  $defaults = simply_hired_jobamatic_get_defaults();
  foreach ($defaults as $key => $value) {
    $defaults[$key] = $form_state['values'][$key];
  }

  variable_set('simply_hired_jobamatic_defaults', $defaults);

  $page_title = trim($form_state['values']['simply_hired_jobamatic_page_title']) == '' ? t('Jobs') : $form_state['values']['simply_hired_jobamatic_page_title'];
  variable_set('simply_hired_jobamatic_page_title', $page_title);

  if ($pshid == '' || $jbd == '') {
    drupal_set_message(t('Job-a-Matic API requires both a publisher ID and a Job-a-matic domain. Requests will be aborted without these settings.'), 'warning');
  }
}
